import { call, select, takeEvery } from 'redux-saga/effects';

const identity = state => state;
const save = state => window.localStorage.setItem('persisted-state', JSON.stringify(state))

function* persistState() {
    const state = yield select(identity)
    yield call(save, state)
}

export default function* saveNote() {
    yield takeEvery('*', persistState);
}
