import { createStore, applyMiddleware, compose } from 'redux';
import rootReducer from '../reducers/';
import createSagaMiddleware from 'redux-saga';

import saveNote from './../sagas/sagas'

const sagaMiddleware = createSagaMiddleware();
const composeEnhancers = typeof window === 'object' && window['__REDUX_DEVTOOLS_EXTENSION_COMPOSE__'] ?
  window['__REDUX_DEVTOOLS_EXTENSION_COMPOSE__']({}) :
  compose;

const enhancer = composeEnhancers(applyMiddleware(sagaMiddleware));
const persistedState = localStorage.getItem('persisted-state') ?
  JSON.parse(localStorage.getItem('persisted-state')) :
  {};

export const store = createStore(rootReducer, persistedState, enhancer);

sagaMiddleware.run(saveNote);
