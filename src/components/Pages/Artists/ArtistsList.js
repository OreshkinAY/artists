import React from "react";
import { NavLink } from 'react-router-dom';

const ArtistsLists = ({ item, styles }) => {
  const { _id, firstName, lastName } = item;

  return (<NavLink
    key={_id}
    to={`/artists/${_id}`}
    className={styles['artist-row']}
    activeClassName={styles['active-artist-row']}
  >
    <div
      className={styles['artist-avatar']}
      style={{
        backgroundImage: `url(/imgs/artists/${_id}.png)`
      }}
    />
    {<span>{`${firstName} ${lastName}`}</span>}
  </NavLink>);
}

export default ArtistsLists;