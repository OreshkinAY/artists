import React, { useState } from "react";

import NavBar from "../../NavBar";
import Dashboard from "../../Dashboard";
import ArtworksList from "./ArtworksList";

// consider this data arrived from the API
// <donotmodify>
const artworks = [
  { _id: "1", title: "In the event of sinking" },
  { _id: "2", title: "Refugees in a nutshell" },
  { _id: "3", title: "Solidarity" },
  { _id: "4", title: "The rehearsal" }
];
// </donotmodify>

export default () => {
  const [selectedArtworkId, selectArtwork] = useState(null);
  const artworkClick = id => () => selectArtwork(id);

  return (
    <div>
      <NavBar />
      <Dashboard
        data={artworks}
        page='artworks'
        selectId={selectedArtworkId}
      >
        <ArtworksList
          artworkClick={artworkClick}
          artworkSelectId={selectedArtworkId}
        />
      </Dashboard>
    </div>
  );
}
