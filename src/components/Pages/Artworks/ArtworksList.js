import React from "react";
import cn from 'classnames';

const ArtworksLists = ({ item, styles, ...props }) => {
  const { _id, title } = item;
  const { artworkClick, artworkSelectId } = props;
  const rowClass = cn({
    [`${styles['artist-row']}`]: true,
    [`${styles['active-artist-row']}`]: artworkSelectId === _id,
  });

  return (
    <div
      className={rowClass}
      onClick={artworkClick(_id)}
    >
      <div
        className={styles['artist-avatar']}
        style={{
          backgroundImage: `url(/imgs/artists/${_id}.png)`
        }}
      />
      {<span>{title}</span>}
    </div>
  );
}

export default ArtworksLists;