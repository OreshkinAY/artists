import React from "react";
import styles from "./styles.module.css";

class Dashboard extends React.Component {
  render() {
    return (
      <div className={styles['dashboard']}>
        <div>
          <h3 className={styles['header']}>{this.props.page}</h3>
          {this.renderArtistList()}
        </div>
        {this.renderSpotLight()}
      </div>
    );
  }

  renderArtistList = () => {
    return this.props.data.map(item => {
      return (
        React.cloneElement(
          this.props.children,
          {
            key: `${item._id}`,
            item,
            styles,
            ...this.props
          }
        )
      )
    });
  }

  renderSpotLight() {
    const props = this.props;
    if (!props.selectId) {
      return null;
    }
    const inSpotlight = props.data.find(
      item => item._id === props.selectId
    );
    const label =  inSpotlight.title || `${inSpotlight.firstName} ${
      inSpotlight.lastName
      }`;
    const imgUrl = `imgs/artists/${inSpotlight._id}.png`;
    return (
      <div className={styles['spotlight']}>
        <div
          className={styles['spotlight-img']}
          style={{
            backgroundImage: `url(${imgUrl})`
          }}
        />
        <div className={styles['spotlight-label']}>{label}</div>
      </div>
    );
  }
}

export default Dashboard;
